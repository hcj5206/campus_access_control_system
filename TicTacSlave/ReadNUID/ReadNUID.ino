/*
 * 
 */

#include <SPI.h>
#include <MFRC522.h>
#include <SoftwareSerial.h>
#include <Servo.h>
#define SS_PIN 10
#define RST_PIN 9
Servo myservo;  // 定义Servo对象来控制
MFRC522 rfid(SS_PIN, RST_PIN); // Instance of the class
MFRC522::MIFARE_Key key; 

SoftwareSerial SerialMy(2,3);//RX=2,TX=3
// Init array that will store new NUID 
byte nuidPICC[4];
String CardIDGobal=""; // 存储id卡号
int pos = 0;    // 角度存储变量
void setup() { 
   
  Serial.begin(9600);  //硬件串口
  SerialMy.begin(9600);
  myservo.attach(6);  // 控制线连接数字9
   myservo.write(0);  
  SPI.begin(); // Init SPI bus
  rfid.PCD_Init(); // Init MFRC522 
//  SerialMy.println("SerialMy");
  Serial.println("Serial");
  for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
  }
  
}
 
void loop() {
    if(SerialMy.available()>0)
  {
    String ch =SerialMy.readStringUntil('\n');
    Serial.print(ch);
    int a=ch.indexOf("<");
    int b=ch.indexOf(">");
    if(a!=-1&&b!=-1){
      String code=ch.substring(a+1,b);
//      RecvFlag=true;
      ControlMode(code.toInt());
      Serial.print(code);
      Serial.print("ok");
//      SerialMy.print("ok");
    }
  }
  // Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
  if ( ! rfid.PICC_IsNewCardPresent())
    return;

  // Verify if the NUID has been readed
  if ( ! rfid.PICC_ReadCardSerial())
    return;


  bool RecvFlag=false;
  String cardid=readcard();
  if (cardid!=""){
      CardIDGobal=cardid;
      SendCodeToServer(cardid+"@");
  }

 
}
void SendCodeToServer(String code){
  String str="("+code+")%";
  Serial.println("Send:"+str);
  SerialMy.print(str);
}
String readcard(){
  String cardid="";
  MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
  // Check is the PICC of Classic MIFARE type
  if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&  
    piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
    piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
    Serial.println(F("Your tag is not of type MIFARE Classic."));
    return;
  }
    if (rfid.uid.uidByte[0] != nuidPICC[0] || 
    rfid.uid.uidByte[1] != nuidPICC[1] || 
    rfid.uid.uidByte[2] != nuidPICC[2] || 
    rfid.uid.uidByte[3] != nuidPICC[3] ) {
    Serial.println(F("A new card has been detected."));

    // Store NUID into nuidPICC array
    
    for (byte i = 0; i < 4; i++) {
      nuidPICC[i] = rfid.uid.uidByte[i];
      cardid=cardid+ nuidPICC[i];
    }

  }
  else Serial.println(F("Card read previously."));
    return cardid;
}

void openDoor(){
  // 0-90 开门
  SendCodeToServer("opendoor");
    for (pos = 0; pos <=90; pos ++) { // 0°到180°
    // in steps of 1 degree
    myservo.write(pos);              // 舵机角度写入
    delay(5);                       // 等待转动到指定角度
  }
   delay(3000); 
   closeDoor();
}
void closeDoor(){
  // 90-0
//   SendCodeToServer("closedoor");

  for (pos = 90; pos >= 0; pos --) { // 0°到180°
    // in steps of 1 degree
    myservo.write(pos);              // 舵机角度写入
    delay(5);                       // 等待转动到指定角度
  }
    Serial.println(pos);
   delay(500);
   SendCodeToServer("closedoor");
   ClearCardID();
}
//清空id
void ClearCardID(){
    for (byte i = 0; i < 4; i++) {
      nuidPICC[i] = 0;
      }
}
//控制舵机模式
bool ControlMode(int x){
    if (x==1) //take chess from board 1
      {
        openDoor();
    }
    if (x==2) //take chess from board 2
      {
        
        closeDoor();
      
    }
    

}
