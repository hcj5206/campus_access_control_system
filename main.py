from main_gui import *


class MyApp(wx.App):
    def OnInit(self):
        mainwin = MyFrame1(None)
        mainwin.CenterOnParent(wx.BOTH)
        mainwin.Show()
        mainwin.Center(wx.BOTH)
        return True


if __name__ == "__main__":
    app = MyApp()
    app.MainLoop()